class Document < ApplicationRecord
  has_many_attached :attachment
  acts_as_commentable
  before_create :generate_fields
  before_save :copy_reference_division

  has_many :user_documents, :dependent => :destroy
  has_many :users, through: :user_documents

  def copy_reference_division
    self.reference_division = self.destination_department
  end 


  def reference_string
    reference_division.to_s + reference_document_no.to_s
  end
  def generate_fields
    if Document.count > 0 
      self.document_no = Document.last.document_no + 1
      # if self.document_no < 9000
      #   self.document_no = Document.last.document_no + 9001
      # end
    else
      # self.document_no = 1
      self.document_no = 15500
    end

    self.document_date = Date.today
  end
end
