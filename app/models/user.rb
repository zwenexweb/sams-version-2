class User < ApplicationRecord
  include Jwt
  include Sms
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :user_departments, :dependent => :destroy
  has_many :departments, through: :user_departments

  has_many :user_documents, :dependent => :destroy
  has_many :documents, through: :user_documents



  def self.user_types
    ['super_admin','admin', 'editor', 'viewer']
  end 

  def self.get_admin
    where(user_type: "admin")
  end 

  def is_admin?
    user_type == "admin"
  end

  def is_super_admin?
    user_type == "super_admin"
  end

  def is_normal_user?
    user_type != "admin" && user_type != "super_admin"
  end 

    def  valid_otp? otp
    if DateTime.now < self.otp_expires_at.localtime and self.otp == otp
      self.update_attributes(:api_token => Jwt.encode_token({:phone => self.phone,:user_id => self.id , :token => Devise.friendly_token}))
      return true
    end
    return false
  end
    
  def send_otp
    otp = self.otp
    if self.otp_expires_at.nil? or  DateTime.now > self.otp_expires_at.localtime
      otp = self.generate_otp
      self.update_attributes(:otp => otp,:otp_expires_at => DateTime.now + 15.minutes )
      p self.errors
    end
    request_otp(self.phone,otp)
  end


end
