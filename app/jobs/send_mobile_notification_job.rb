require 'fcm'

class SendMobileNotificationJob < ApplicationJob
  queue_as :default

  def perform(message, registration_id, document_id, type)
    fcm_client = FCM.new("AAAArUUYex4:APA91bFQ2nXYtGv3v0vmi8SiKtufc6lM82pYxVoggTD5xDekDDTL2OtcY1rEcPRO6UQuXne3sSnU2E6zxx54uDjdVLXA17g1PkV7Cj4W7gbUgsHsZMBJoXJ0V_5HW74P7iZto8PtFrX2") # FCM_SERVER_KEY

    if type == 'comment'
      title = "New Comment"
    elsif type == 'new_document'
      title = "New Document"
    end 

    options = { priority: 'high',
                  data: { message: message, icon: 'image',document_id: document_id, click_action: "FLUTTER_NOTIFICATION_CLICK" },
                  notification: { body: message,
                                  title: title,
                                  sound: 'default',
                                  icon: 'image.png',
                                  document_id: document_id
                              }
                }
     response = fcm_client.send(registration_id, options)
    puts response

  end
end

