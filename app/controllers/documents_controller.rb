class DocumentsController < ApplicationController
  include NotificationSender
  before_action :authenticate_user!
  before_action :set_document, only: [:show, :edit, :update, :destroy, :create_comments]
  before_action :check_authorized_editor, except: [:index, :new, :create]
  before_action :check_document_created_user, only: [:edit]

  # GET /documents
  # GET /documents.json
  def index
    @documents = Document.all.order(created_at: :desc)
    @documents_json = DocumentSerializer.new(@documents, params: {current_user: current_user } ).serialized_json
  end

  def create_comments
    @document = Document.find(params[:comment][:document_id])
    comment = @document.comments.create

    comment.title = params[:comment][:title]
    comment.comment = params[:comment][:comment]
    comment.user_id = current_user.id
    comment.save

    if @document.comments.map{|x| User.find(x.user_id).user_type}.include?("admin")
      admin_mobile_registration_ids = []
      @document.comments.map{|x| admin_mobile_registration_ids << User.find(x.user_id) if User.find(x.user_id).user_type == "admin" && User.find(x.user_id) != current_user.id}

      mobile_registration_ids = admin_mobile_registration_ids.uniq.pluck(:mobile_device_token) + (@document.users.pluck(:mobile_device_token) - [current_user.mobile_device_token])
      web_push_registration_ids = admin_mobile_registration_ids.uniq.pluck(:web_push_token) + (@document.users.pluck(:web_push_token) - [current_user.web_push_token])
    else
      mobile_registration_ids = (@document.users.pluck(:mobile_device_token) - [current_user.mobile_device_token])
      web_push_registration_ids = (@document.users.pluck(:web_push_token) - [current_user.web_push_token])
    end
    # mobile_registration_ids = User.get_admin().pluck(:mobile_device_token) + (@document.users.pluck(:mobile_device_token) - [current_user.mobile_device_token])
    
    # web_push_registration_ids = User.get_admin().pluck(:web_push_token) + (@document.users.pluck(:web_push_token) - [current_user.web_push_token])
    

    message = "#{current_user.name} commented on document number #{@document.document_no}"

    send_fcm_push_notification(message, mobile_registration_ids, @document.id, 'comment')
    send_fcm_push_notification(message, web_push_registration_ids, @document.id, 'comment')
    # NotificationContent.create(noti_message: message, details_id: @document.id, user_ids: @document.created_user)
    @notification_content = NotificationContent.new
    @notification_content.noti_message = message
    @notification_content.details_id = @document.id
    # @notification_content.user_ids = @notification_content.user_ids << [@document.created_user] - [current_user.id]
    # @notification_content.user_ids = @notification_content.user_ids << @document.created_user
    @notification_content.user_ids << @document.created_user
    @notification_content.user_ids << @document.comments.last(2).first.user_id
    @notification_content.user_ids = @notification_content.user_ids - [@current_user.id]
    @notification_content.save
    redirect_to @document,  notice: 'Comment was successfully created.'
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
  end

  # GET /documents/new
  def new
    @document = Document.new
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)
    if current_user.is_normal_user? && !@document.user_ids.include?(current_user.id)
      @document.user_ids = @document.user_ids << current_user.id
    end

    respond_to do |format|
      if @document.save
        mobile_registration_ids = @document.users.pluck(:mobile_device_token) - [current_user.mobile_device_token]
        web_push_registration_ids = @document.users.pluck(:web_push_token) - [current_user.web_push_token]
        message = "#{current_user.name} created a new document ( No-#{@document.document_no} )"
        
        

        send_fcm_push_notification(message, mobile_registration_ids, @document.id, "new_document")
        send_fcm_push_notification(message, web_push_registration_ids, @document.id, "new_document")

        NotificationContent.create(noti_message: message, details_id: @document.id, user_ids: @document.user_ids - [current_user.id])

        format.html { redirect_to @document, notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        if current_user.is_normal_user? && !@document.user_ids.include?(current_user.id)
          @document.user_ids = @document.user_ids << current_user.id
        end
        format.html { redirect_to @document, notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def check_authorized_editor
      @document = Document.find(params[:comment][:document_id]) if params[:comment].present?
      if current_user.is_normal_user? && !@document.user_ids.include?(current_user.id)
      # if @document.created_user != current_user.id
        redirect_to root_path
      end
    end 

    def set_document
      @document = Document.find(params[:id]) if params[:id].present?
    end

    # Only allow a list of trusted parameters through.
    def document_params
      params.require(:document).permit(:document_date, :document_no, :division, :sub_division, :reference_document_no, :destination_department, :subject,  :working_status, :reference_division, :created_user, attachment: [], user_ids: [])
    end

    # def notification_content_params
    #   params.require(:notification_content).permit(:noti_message, :details_id, user_ids: [])
    # end

    def check_document_created_user
      if @document.created_user != current_user.id
        redirect_to root_path
      end
    end
end
