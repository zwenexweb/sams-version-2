class DocumentSerializer
  include FastJsonapi::ObjectSerializer


  attributes :id,:document_date,:reference_document_no, :division,:sub_division, :reference_document_no,:destination_department,:subject,:working_status, :reference_division,:document_no
  attribute :html do |o,params|
    {
      id: o.id.to_s.to_mm,
      actions: html_actions(o,params[:current_user]),
      document_date: o.document_date.to_s.to_mm,
      attachment: html_link("Attachment",o.attachment.attachments,o)
    }
  end

  
  def self.html_actions obj,current_user
      if Rails.env.production?
        (ApplicationController.renderer.new({ http_host: "http://ogmail.37deus.cloud", port: 80}).render(partial: "shared/documents/actions", :locals => { obj: obj, current_user: current_user}))
      else
        (ApplicationController.renderer.new({ http_host: "http://localhost", port: 3000 }).render(partial: "shared/documents/actions", :locals => { obj: obj , current_user: current_user}))
      end
  end


  def self.html_link name,obj,params
    if obj.present?
      if Rails.env.production?
        (ApplicationController.renderer.new({ http_host: "http://ogmail.37deus.cloud", port: 80}).render(partial: "shared/link", :locals => { name: name, obj: obj, params: params }))
      else
        (ApplicationController.renderer.new({ http_host: "http://localhost", port: 3000 }).render(partial: "shared/link", :locals => { name: name, obj: obj, params: params }))
      end
    end
  end
  
end
