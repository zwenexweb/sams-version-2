json.merge! api_v1_document.attributes

json.comments api_v1_document.comments do |comment|
  json.merge! comment.attributes

  json.user if json.user.present? do 
    json.name comment.user.name
    json.email comment.user.email
    #json.phone comment.user.phone
  end
                             
end

json.attachments api_v1_document.attachment.attachments do |attachment|
    json.url url_for(attachment)
end 
