import {MDCList} from "@material/list";
import { Controller } from "stimulus"
import {MDCTopAppBar} from "@material/top-app-bar";
import {MDCDrawer} from "@material/drawer";

export default class extends Controller{
    connect(){
	const list = MDCList.attachTo(document.querySelector('.mdc-list'));
	list.wrapFocus = true;
	const drawer = MDCDrawer.attachTo(document.querySelector('.mdc-drawer'));
	const topAppBar = MDCTopAppBar.attachTo(document.getElementById('app-bar'));
	topAppBar.setScrollTarget(document.getElementById('main-content'));
	topAppBar.listen('MDCTopAppBar:nav', () => {
	    drawer.open = !drawer.open;
	});
    }
}
