import { Controller } from "stimulus"

export default class extends Controller {
    

    connect() {
	console.log("Connected")


	$(this.id).autocomplete({
	    source: this.list
	});
	

    }

    get list(){
	if(this.isSubDivision){
	    return this.subdivisions;
	}	
    }

    get subdivisions() {
	return [
	    "၁-က",
	    "၁-ခ",
	    "၁-ဂ",
	    "၂-က",
	    "၂-ခ",
	    "၂-ဂ",
	    "၃-က",
	    "၃-ခ",
	    "၃-ဂ",
	    "၄-က",
	    "၄-ခ",
	    "၅-က",
	    "၆-က",
	    "၇-က",
	    "၇-ခ",
	    "၈-က",
	    "၈-ခ",
	    "၉-က",
	    "၉-ခ",
	    "၉-ဂ",
	    "၁၀-က",
	    "၁၀-ခ",
	    "၁၁-က",
	    "၁၁-ခ",
	    "၁၂-က",
	    "၁၂-ခ",
	    "၁၃-က",
	    "၁၃-ခ",
	    "၁၄-က",
	    "၁၅-က",
	    "၁၆-က",
	    "၁၇-က",
	    "၁၈-က",
	    "၁၉-က",
	    "၁၉-ခ",
	    "၂၀-က",
	    "၂၁-က",
	    "၂၂-က",
	    "၂၃-က",
	    "၂၃-ခ",
	    "၂၄-က",
	    "၂၄-ခ",
	    "၂၄-ဂ",
	    "၂၅-က",
	    "၂၆-က",
	    "၂၇-က",
	    "၂၇-ခ",
	    "၂၇-ဂ",
	    "၂၈-က",
	    "၂၈-ခ",
	    "၂၈-ဂ"
	];
    }
    
    get id() {
	return this.data.get("id")
    }

    get isSubDivision() {
	return this.data.get("type") == "subdivision"
    }
}

