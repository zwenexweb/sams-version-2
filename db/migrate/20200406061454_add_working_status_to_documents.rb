class AddWorkingStatusToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :working_status, :string
  end
end
