class RemoveUseIdFromNotificationContent < ActiveRecord::Migration[6.0]
  def change

    remove_column :notification_contents, :user_id, :integer
  end
end
