class AddUserIdsToNotificationContent < ActiveRecord::Migration[6.0]
  def change
    add_column :notification_contents, :user_ids, :integer, array: true, default: []
  end
end
