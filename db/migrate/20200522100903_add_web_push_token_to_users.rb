class AddWebPushTokenToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :web_push_token, :string
  end
end
