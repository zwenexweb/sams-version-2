require "application_system_test_case"

class Api::V1::DocumentsTest < ApplicationSystemTestCase
  setup do
    @api_v1_document = api_v1_documents(:one)
  end

  test "visiting the index" do
    visit api_v1_documents_url
    assert_selector "h1", text: "Api/V1/Documents"
  end

  test "creating a Document" do
    visit api_v1_documents_url
    click_on "New Api/V1/Document"

    click_on "Create Document"

    assert_text "Document was successfully created"
    click_on "Back"
  end

  test "updating a Document" do
    visit api_v1_documents_url
    click_on "Edit", match: :first

    click_on "Update Document"

    assert_text "Document was successfully updated"
    click_on "Back"
  end

  test "destroying a Document" do
    visit api_v1_documents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Document was successfully destroyed"
  end
end
