require "application_system_test_case"

class NotificationContentsTest < ApplicationSystemTestCase
  setup do
    @notification_content = notification_contents(:one)
  end

  test "visiting the index" do
    visit notification_contents_url
    assert_selector "h1", text: "Notification Contents"
  end

  test "creating a Notification content" do
    visit notification_contents_url
    click_on "New Notification Content"

    fill_in "Details", with: @notification_content.details_id
    fill_in "Noti message", with: @notification_content.noti_message
    fill_in "User", with: @notification_content.user_id
    click_on "Create Notification content"

    assert_text "Notification content was successfully created"
    click_on "Back"
  end

  test "updating a Notification content" do
    visit notification_contents_url
    click_on "Edit", match: :first

    fill_in "Details", with: @notification_content.details_id
    fill_in "Noti message", with: @notification_content.noti_message
    fill_in "User", with: @notification_content.user_id
    click_on "Update Notification content"

    assert_text "Notification content was successfully updated"
    click_on "Back"
  end

  test "destroying a Notification content" do
    visit notification_contents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Notification content was successfully destroyed"
  end
end
