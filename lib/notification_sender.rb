module NotificationSender
    def send_fcm_push_notification(message, registration_ids, document_id, type)
        if Rails.env == 'production'
            registration_ids.compact.each_slice(50) do |registration_id|
                SendMobileNotificationJob.perform_later(message, registration_id, document_id, type)
            end
        end
    end
end 