# coding: utf-8
require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

class String
  def hasherizer
    self.split(".").reverse.inject{|a,n| {n=>a}} 
  end

  def to_normalized_excel_boolean
    if ActiveModel::Type::Boolean.new.cast(self) == true
      "Y"
    else
      "N"
    end
  end

  def to_mm
    self.gsub(/0/, '၀')
      .gsub(/1/, '၁')
      .gsub(/2/, '၂')
      .gsub(/3/, '၃')
      .gsub(/4/, '၄')
      .gsub(/5/, '၅')
      .gsub(/6/, '၆')
      .gsub(/7/, '၇')
      .gsub(/8/, '၈')
      .gsub(/9/, '၉')
  end
end

module MoeRegistry
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0
    config.autoload_paths += %W(#{config.root}/lib)
    config.active_job.queue_adapter = :sidekiq
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
